import { useContext } from 'react'
import logo from '../logo.svg';
import '../App.css';
import { Link } from 'react-router-dom';
import useStore from '../custom-hook/useStore'
import NavBar from '../components/NavBar';
import CardName from '../components/CardName';

function Home() {
    const [state, setState] = useStore();
    const papa = "paparulo";
    const paparulo = () => {
        setState(papa);

    }


    return (<>
        <NavBar/>
        <div className="App">
            <header className="App-header">
                    usuario: {state.username}
                    <CardName/>
                    
            </header>
            <button onClick={paparulo}>Boton de paparulos</button>
        </div>
    </>
    );
}

export default Home;
