import React from 'react';
import { useContext } from 'react'
import { Context } from '../store/store'

const useStore = () => {
    const [state, dispatch] =  useContext(Context);
    
    const setState = (newState)=>{
        dispatch({type:"UPDATE_NAME", payload:newState })
    }

    return [state,setState];
}

export default useStore;