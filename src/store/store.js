import React,{createContext,useContext, useReducer} from "react";
import Reducer from './reducer'

const initialState = {
    username:"Nombre de usuario"
}

const Store = ({children})=>{
    const [state, dispatch] = useReducer(Reducer, initialState);
    return(
        <Context.Provider value={[state, dispatch]}>
            {children}
        </Context.Provider>
    )
}

export const Context = createContext(initialState)
export default Store;