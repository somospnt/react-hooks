import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import Home from './home/Home.js'
import Store from "./store/store.js";

function App() {
  return (
    <Store>
      <BrowserRouter>
        <Switch>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </BrowserRouter>
    </Store>
  );
}

export default App;
