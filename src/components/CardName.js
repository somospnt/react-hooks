import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import useStore from '../custom-hook/useStore';

const CardName = () => {
    const [store, setStore] = useStore();
    const actualizarUserName = (event) => {
        setStore(event.target.value)

    }
    return <>
        <InputGroup className="mb-3 ">
            <InputGroup.Text>First name</InputGroup.Text>
            <FormControl aria-label="First name" onChange={actualizarUserName} />
        </InputGroup>
    </>
}
export default CardName;