import Nav from 'react-bootstrap/Nav'
import { FaUserCircle } from 'react-icons/fa';
import useStore from '../custom-hook/useStore';

const NavBar = () => {
    const [store, setStore] = useStore()
    return <>
        <Nav style=
            {{
                display: "flex",
                justifyContent: "right",
                marginBottom: "10px",
                marginTop: "10px",
                marginRight: "40px"
            }}
            activeKey="/home"
            onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
        >

            <Nav.Item>
                <FaUserCircle />
                {store.username}
            </Nav.Item>
        </Nav>
    </>
}

export default NavBar;